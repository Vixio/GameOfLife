package com.lv.game;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by christian on 14/09/15.
 */
public class GameOfLife {
    private Scanner scanner;
    private int currentGen;
    private int totalGen;
    private Board board;

    public GameOfLife() {
        scanner    = new Scanner(System.in);
        currentGen = 0;
        totalGen   = 0;
    }

    public void run() {
        initializeBoard();

        scanner.nextLine();

        System.out.println("Estado inicial del tablero: ");
        System.out.println(board);

        while ( ! board.isEmpty() && currentGen < totalGen ) {
            currentGen++;
            System.out.println("Paso " + currentGen);

            board.step();
            System.out.println(board);

            System.out.println("Presiona enter para continuar la simulacion...");
            scanner.nextLine();
        }

        System.out.println("Finalizo la simulación");
    }

    private void initializeBoard() {
        int R      = nextInt("Número de filas del tablero: ", 2, 50, "El número de filas debe de ser un valor en un rango de 2 a 20");
        int C      = nextInt("Número de columnas del tablero: ", 2, 50, "El número de columnas debe de ser un valor en un rango de 2 a 20");
        totalGen   = nextInt("Número de generaciones del juego: ", 1, 50, "El número de generaciones debe de ser un valor en un rango del 1 al 50");
        int numOrg = nextInt("Número de organismos iniciales: ", 0, (R * C / 2), "El número de organimos debe de ser mayor que 0 y no debe ser mayor de " + (R * C / 2));

        board = new Board(R, C);
/*
        scanner.nextLine();
        for ( int i = 0; i < R; i++ ) {
            String line = scanner.nextLine();

            if ( line.isEmpty() ) {
                break;
            }

            for ( int j = 0; j < line.length(); j++ ) {
                if ( line.charAt(j) == '*' ) {
                    board.setOrganisms(i, j);
                }
            }
        }
*/

        for ( int i = 0; i < numOrg; i++ ) {
            int row = nextInt("Fila del organismo " + i + ": ", 0, R - 1, "La fila debe ser un valor de 0 a " + (R - 1));
            int col = nextInt("Columna de organismo " + i + ": ", 0, R - 1, "La columna debe ser un valor de 0 a " + ( R - 1 ));

            board.setOrganisms(row, col);
        }
    }

    private int nextInt(String message, int minValue, int maxValue, String errorMessage) {
        int value;
        do {
            System.out.print(message);

            try {
                value = scanner.nextInt();
            } catch (InputMismatchException e) {
                scanner.nextLine();
                System.out.println("Debe ingresar una cantidad numerica");
                continue;
            }

            if ( value >= minValue && value <= maxValue ) {
                break;
            }

            System.out.println(errorMessage);
        } while ( true );

        return value;
    }
}
