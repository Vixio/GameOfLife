package com.lv.game;

/**
 * Created by christian on 14/09/15.
 */
public class Organism {
    enum State {
        ALIVE,
        DEAD
    }

    private int row;
    private int column;

    private Board board;
    private State nextState;
    private State state;

    public Organism(Board board, int row, int column) {
        this.board   = board;
        this.row     = row;
        this.column  = column;
        this.state   = State.DEAD;
    }

    public void calculateNextState() {
        // Count the number of neighbors adjacents
        int dr[] = { -1, -1, -1,  0, 0,  1, 1, 1};
        int dc[] = { -1,  0,  1, -1, 1, -1, 0, 1};

        int numNeighbors = 0;
        for ( int i = 0; i < dr.length; i++ ) {
            int neighRow = row + dr[i];
            int neighCol = column + dc[i];

            if ( neighRow >= 0 && neighRow < board.getHeight() && neighCol >= 0 && neighCol < board.getWidth() ) {
                if ( board.getOrganism(neighRow, neighCol).isAlive() ) {
                    numNeighbors++;
                }
            }
        }

        if ( isAlive() ) {
            if ( numNeighbors < 2 || numNeighbors > 3 ) {
                this.nextState = State.DEAD;
            }
        }
        else if ( numNeighbors == 3 ) {
            this.nextState = State.ALIVE;
        }
    }

    public void applyNextState() {
        this.state = nextState;
    }

    public boolean isAlive() {
        return state == State.ALIVE;
    }

    public void setIsAlive(boolean isAlive) {
        if ( isAlive ) {
            this.state = State.ALIVE;
        } else {
            this.state = State.DEAD;
        }

        this.nextState = this.state;
    }
}
