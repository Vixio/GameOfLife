package com.lv.game;

/**
 * Created by christian on 14/09/15.
 */
public class Board {
    private Organism[][] organisms;

    public Board(int rows, int columns) {
        initializeBoard(rows, columns);
    }

    /**
     * Initialize the matrix of organism with dead organism
     * @param rows The number of rows of the matrix
     * @param columns The number of columns of the matrix
     */
    private void initializeBoard(int rows, int columns) {
        organisms = new Organism[rows][columns];

        for ( int i = 0; i < rows; i++ ) {
            for ( int j = 0; j < columns; j++ ) {
                organisms[i][j] = new Organism(this, i, j);
            }
        }
    }

    /**
     * Set an organism alive in a given position
     * @param row The row where the organism is
     * @param column The column where the organism is
     */
    public void setOrganisms(int row, int column) {
        organisms[row][column].setIsAlive(true);
    }

    public void step() {
        for ( int i = 0; i < organisms.length; i++ ) {
            for ( int j = 0; j < organisms[i].length; j++ ) {
                organisms[i][j].calculateNextState();
            }
        }

        for ( int i = 0; i < organisms.length; i++ ) {
            for ( int j = 0; j < organisms[i].length; j++ ) {
                organisms[i][j].applyNextState();
            }
        }
    }

    /**
     * Return true if all the organism are daed
     * @return True if all the organism in the board are dead
     */
    public boolean isEmpty() {
        for ( int i = 0; i < organisms.length; i++ ) {
            for ( int j = 0; j < organisms[i].length; j++ ) {
                if ( organisms[i][j].isAlive() ) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Retrieves a organism in a given row and column
     * @param row The row of the organism
     * @param col The column of the organism
     * @return The organism in the position (row, col)
     */
    public Organism getOrganism(int row, int col) {
        return organisms[row][col];
    }

    /**
     * Get the width of the board
     * @return Width of the board
     */
    public int getHeight() {
        return organisms.length;
    }

    /**
     * Get the height of the board
     * @return Height of the board
     */
    public int getWidth() {
        return organisms[0].length;
    }

    @Override
    public String toString() {
        StringBuilder boardString = new StringBuilder();

        for ( int i = 0; i < organisms.length; i++ ) {
            for ( int j = 0; j < organisms[i].length; j++ ) {
                boardString.append( organisms[i][j].isAlive() ? "[*]" : "[ ]");
            }

            boardString.append('\n');
        }

        return boardString.toString();
    }
}
